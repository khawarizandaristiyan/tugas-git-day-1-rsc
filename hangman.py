import csv
import random

dict_kata = {}
akun_aktif = {}
banyak_akun = 0
skor = 0
akun_lawan = -1
poin2P, skor2P = [0, 0], [0, 0]
bonus1 = True
bonus2 = False
bonus4 = True

# Konstanta
maksRonde2P = 5

def banding(entri1, entri2):
    # return skor entri1 lebih dari skor entri2 atau username entri1 alfabetis lebih dulu dibanding username entri2
    if(int(entri1["skor"]) > int(entri2["skor"])):
        return True
    elif(int(entri1["skor"]) < int(entri2["skor"])):
        return False
    else:
        return entri1["username"] < entri2["username"]
        
def tukar_entri(entri1, entri2):
    # KAMUS lokal:
    # temp : int
    # entri1, entri2 : dictionary

    list_akun[entri1["index"]] = entri2
    list_akun[entri2["index"]] = entri1
    temp = entri1["index"]
    entri1["index"] = entri2["index"]
    entri2["index"] = temp
    return


def refresh_list_akun(banyak_entri_terambil):
    # KAMUS lokal:
    # banyak_entri_terambil : int
    # entri : dictionary

    global list_akun, banyak_akun
    banyak_akun += banyak_entri_terambil
    for entri in list_akun:
        sortir_naik(entri)
    return

def elemen_pertama():
    global list_akun, banyak_akun
    elemen = list_akun[0]
    banyak_akun -= 1
    tukar_entri(list_akun[0], list_akun[banyak_akun])
    sortir_turun(list_akun[0])
    return elemen


def sortir_naik(entri):
    global list_akun
    naik = (entri["index"] - 1) // 2
    while (naik >= 0):
        if (not banding(list_akun[naik], entri)):
            tukar_entri(entri, list_akun[naik])
        else:
            return
        naik = (entri["index"] - 1) // 2


def sortir_turun(entri):
    global list_akun, banyak_akun
    while (True):
        anak1 = entri["index"] * 2 + 1
        anak2 = anak1 + 1
        if (anak1 < banyak_akun):
            swap = anak1
            if (anak2 < banyak_akun):
                if (banding(list_akun[anak2], list_akun[anak1])):
                    swap = anak2
            if (banding(list_akun[swap], entri)):
                tukar_entri(entri, list_akun[swap])
            else:
                return
        else:
            return
def open_data_kata():
    global dict_kata, max_kata
    dict_kata = {}  # Inisialisasi variabel
    with open('Data kata.csv') as csv_file:  # Algoritma untuk membuka file eksternal (.csv)
        csv_reader = csv.reader(csv_file, delimiter=',')
        kategori = "???"
        for row in csv_reader:  # Memasukkan isi file eksternal ke dalam list
            if (len(row) > 0):
                # Format isi file Data kata:
                # awalan '#' = komentar
                # awalan ':' = kategori
                # tanpa awalan = entri
                if (row[0].startswith("#")): continue
                if (row[0].startswith(":")): kategori = row[0][1:].upper()
                if (not kategori in dict_kata.keys()):
                    dict_kata[kategori] = []
                else:
                    dict_kata[kategori].append(row[0].upper())

        max_kata = 0
        for entri in list(dict_kata.items()):
            max_kata += len(entri)
    return


def open_data_akun():
    global list_akun, list_password, banyak_akun
    list_akun = []  # Inisialisasi variabel
    with open('Data akun.csv') as csv_file:  # Algoritma untuk membuka file eksternal (.csv)
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:  # Memasukkan isi file eksternal ke dalam list
            entri = {
                "index": banyak_akun,
                "username": row[0],
                "password": row[1],
                "skor": row[2]
            }
            list_akun.append(entri)
            sortir_naik(entri)
            banyak_akun += 1
    return

def open_kata_ditebak(fileKata):
    global guessedWord
    guessedWord = []
    try:
        with open(fileKata, mode='r') as csv_file:  # Algoritma untuk membuka file eksternal (.csv)
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:  # Memasukkan isi file eksternal ke dalam list
                for word in row:
                    guessedWord.append(word)
    except FileNotFoundError:
        f = open(fileKata, mode='w')
        f.close()
    return

def save_kata_ditebak(fileKata, akunBaru = False):
    global guessedWord
    with open(fileKata, mode='w', newline='\n') as saved:  # Algoritma untuk membuka file eksternal (.csv)
        csv_writer = csv.writer(saved, delimiter=',')
        if akunBaru:
            guessedWord = []
        csv_writer.writerow(guessedWord)
    return

def save_data_akun():
    global list_akun, banyak_akun
    # Prosedur untuk mengubah data di file eksternal setelah data diubah di dalam program
    with open("Data akun.csv", mode='w', newline='\n') as saved:
        csv_writer = csv.writer(saved, delimiter=',')
        for entri in list_akun:
            csv_writer.writerow([entri["username"], entri["password"], entri["skor"]])
    return


def login(list_kecuali=[]):
    global list_akun, username
    # wrongpass = True
    indeks_akun = -1
    # while wrongpass:
    while (indeks_akun < 0):
        username = input("Masukkan nama anda: ")
        password = input("Masukkan password anda: ")

        for entri in list_akun:
            if (username.upper() == entri["username"].upper() and password == entri["password"]):
                indeks_akun = entri["index"]
                break

        if indeks_akun < 0:
            print("Username atau password salah!\n")
        elif (len(list_kecuali) > 0 and entri["index"] in list_kecuali):
            print("Tidak bisa login ke akun ini!\n")
            indeks_akun = -1

    print("\nBerhasil login!")
    print(f'Selamat datang kembali, {list_akun[indeks_akun]["username"]}')
    open_kata_ditebak("Kata ditebak-{}.csv".format(username))
    return list_akun[indeks_akun]


def register():
    global banyak_akun, list_akun

    usernameSama = True
    username = ''
    while usernameSama:
        username = input("Masukkan nama anda: ")
        if len(list_akun) == 0:
            usernameSama = False
        else:
            for entri in list_akun:
                if username.upper() == entri["username"].upper():
                    print("Username sudah digunakan")
                    break
                else:
                    usernameSama = False

    password = input("Masukkan password anda: ")

    entri = {
        "index": banyak_akun,
        "username": username,
        "password": password,
        "skor": 0
    }
    banyak_akun += 1
    list_akun.append(entri)
    save_data_akun()
    save_kata_ditebak("Kata ditebak-{}.csv".format(username), akunBaru=True)
    print(f'Selamat datang di Hangman, {username}')

def topTen():
    # KAMUS lokal:
    # diambil, i : int
    # row : str
    # entri : dictionary

    global list_akun
    row = '{0:7}{1:20}{2:5}'
    print(row.format('Rank', 'Username', 'Skor'))

    # Tarik nilai minimum karena belum tentu ada 10 atau lebih akun dalam list_akun
    diambil = min(10, len(list_akun))
    for i in range(1, diambil + 1):
        entri = elemen_pertama()
        print(row.format(str(i), entri["username"], str(entri["skor"])))

    # elemen_pertama() mengubah urutan list_akun, maka harus direfresh kembali
    refresh_list_akun(diambil)
    return


def setting():
    # KAMUS lokal:
    # pilihan : str
    # dalamSetting : boolean

    global bonus1, bonus2, bonus4

    dalamSetting = True

    if bonus1 == True:
        print('Hint kategori selama permainan   (A) : ON')
    else:
        print('Hint kategori selama permainan   (A) : OFF')

    if bonus2 == True:
        print('Memilih kategori sebelum bermain (B) : ON')
    else:
        print('Memilih kategori sebelum bermain (B) : OFF')

    if bonus4 == True:
        print('Tampilan spasi dan tanda sambung (C) : ON')
    else:
        print('Tampilan spasi dan tanda sambung (C) : OFF')

    while dalamSetting:
        print('Masukkan input fitur yang ingin anda ubah')
        pilihan = (str(input('Masukkan "Keluar" jika ingin keluar setting: '))).upper()

        if pilihan == 'A' and bonus1 == True:
            bonus1 = False
        elif pilihan == 'A' and bonus1 == False:
            bonus1 = True
            bonus2 = False
        elif pilihan == 'B' and bonus2 == True:
            bonus2 = False
        elif pilihan == 'B' and bonus2 == False:
            bonus2 = True
            bonus1 = False
        elif pilihan == 'C' and bonus4 == True:
            bonus4 = False
        elif pilihan == 'C' and bonus4 == False:
            bonus4 = True
        elif pilihan == 'KELUAR':
            dalamSetting = False
            return
        else:
            print('Pilihan salah!')

        print()

        if bonus1 == True:
            print('Hint kategori selama permainan   (A) : ON')
        else:
            print('Hint kategori selama permainan   (A) : OFF')

        if bonus2 == True:
            print('Memilih kategori sebelum bermain (B) : ON')
        else:
            print('Memilih kategori sebelum bermain (B) : OFF')

        if bonus4 == True:
            print('Tampilan spasi dan tanda sambung (C) : ON')
        else:
            print('Tampilan spasi dan tanda sambung (C) : OFF')

    return


def status():
    global akun_aktif, guessedWord
    print("       Status        ")
    print(f'\nUsername: {akun_aktif["username"]}')
    print(f'Skor    : {akun_aktif["skor"]}')
    print(f'Histori Kata: ')
    print(", ".join(guessedWord))
    return


def kategoriRandom(dictKata):
    # KAMUS lokal:
    # kategori : str
    # dictKata : dictionary

    kategori = random.choice(list(dictKata.keys()))
    return kategori

def kataRandom(dictKata, kategori):
    kata = random.choice(dictKata[kategori])
    return kata

def gambarHangman(salah):
    gambar = [
        '''
         — — — — —
        |
        |
        |
        |
        |
        |
        ''',
        '''
         — — — — —
        |         |
        |
        |
        |
        |
        |
        ''',
        '''
         — — — — —
        |         |
        |         O
        |
        |
        |
        |
        ''',
        '''
         — — — — —
        |         |
        |         O
        |         |
        |
        |
        |
        ''',
        '''
         — — — — —
        |         |
        |         O
        |         |—
        |
        |
        |
        ''',
        '''
         — — — — —
        |         |
        |         O
        |        —|—
        |
        |
        |
        ''',
        '''
         — — — — —
        |         |
        |         O
        |        —|—
        |         |
        |
        |
        ''',
        '''
         — — — — —
        |         |
        |         O
        |        —|—
        |         | 
        |        /
        |
        ''',
        '''
         — — — — —
        |         |
        |         O
        |        —|—
        |         | 
        |        / \\
        |
        '''
    ]
    return gambar[salah]



def mainHangman():
    global skor, bonus1, bonus2, bonus4, guessedWord, username, max_kata

    def bonus_4(progress):
        for i in range(len(kata)):
            if kata[i] == " ":
                progress += " "
            else:
                progress += "_"
        return progress

    def save_data_skor():
        if int(akun_aktif["skor"]) < skor:
            print("Skor anda lebih tinggi dari sebelumnya. Apakah anda ingin menyimpannya? (Ya/Tidak)")
            saveskor = input("> ").upper()
            if saveskor == "YA":
                akun_aktif["skor"] = skor
            elif saveskor == "TIDAK":
                akun_aktif["skor"] = int(akun_aktif["skor"])
            else:
                save_data_skor()
        sortir_naik(akun_aktif)

    menang = True

    while menang:
        # bonus2
        kategori = ''
        if bonus2 == True:
            while True:
                print('             Kategori             ')
                print('Kota, Negara, Hewan, Buah, Sayuran')
                kategori = (str(input('Masukkan kategori yang anda inginkan: '))).upper()
                if(kategori in list(dict_kata.keys())):
                    break
                else:
                    print('Kategori tidak valid!')
        else:
            kategori = kategoriRandom(dict_kata)

        while True:
            kata = kataRandom(dict_kata, kategori)
            if kata not in guessedWord:
                break

        # kata = kataRandom(dict_kata, "namakategoridisini")

        progress = ""

        # bonus4
        if bonus4 == True:
            progress = bonus_4(progress)
        else:
            progress = "_" * len(kata)

        salah = 0
        listHurufSudahDitebak = []

        bonusawal1 = bonus1

        print('Progres kata:', progress)
        print(gambarHangman(salah))

        while salah < 8:
            if bonus1 == True:
                print('Masukkan input "Hint" untuk memunculkan kategori kata')

            huruf = (str(input('Masukkan tebakan huruf anda: '))).upper()

            if len(huruf) == 1:
                if huruf in listHurufSudahDitebak:
                    print('\nMaaf, huruf ini sudah anda tebak')
                    print('Progres kata:', progress)
                    print(gambarHangman(salah))
                    print('List huruf yang sudah anda tebak adalah: ')
                    print(', '.join(listHurufSudahDitebak))
                elif huruf in kata:
                    listHurufSudahDitebak.append(huruf)

                    listProgress = list(progress)
                    ex = [i for i, alfabet in enumerate(kata) if alfabet == huruf]

                    for i in ex:
                        listProgress[i] = huruf
                    progress = ''.join(listProgress)

                    print('\nSelamat! Anda menebak huruf yang benar')
                    print('Progres kata:', progress)
                    print(gambarHangman(salah))
                    print('List huruf yang sudah anda tebak adalah: ')
                    print(', '.join(listHurufSudahDitebak))

                    if "_" not in progress:
                        if bonusawal1 == True:
                            bonus1 = True
                        else:
                            bonus1 = False
                        skor += 100 - (salah ** 2)
                        print('Skor:', skor)

                        guessedWord.append(kata)

                        if len(guessedWord) == max_kata:
                            print("Selamat! Semua kata sudah ditebak")
                            guessedWord.clear()

                        inputSalah = True
                        while inputSalah:
                            print("Lanjutkan permainan? (Ya/Tidak)")
                            lanjut = input("> ").upper()
                            if lanjut == "TIDAK":
                                menang = False
                                inputSalah = False
                            elif lanjut == "YA":
                                menang = True
                                inputSalah = False
                            else:
                                print("Input salah!")
                        break
                else:
                    listHurufSudahDitebak.append(huruf)
                    salah += 1

                    print('\nMaaf, kata yang anda tebak salah')
                    print('Progres kata:', progress)
                    print(gambarHangman(salah))
                    print('List huruf yang sudah anda tebak adalah: ')
                    print(', '.join(listHurufSudahDitebak))
            elif huruf == 'HINT' and bonus1 == True:
                print('Kategori kata adalah', kategori)
                print('Progres kata:', progress)
                print(gambarHangman(salah))
                print('List huruf yang sudah anda tebak adalah: ')
                print(', '.join(listHurufSudahDitebak))
                bonus1 = False
            else:
                print("Hanya menerima input 1 huruf!\n")
                print('Progres kata:', progress)
                print(gambarHangman(salah))
                print('List huruf yang sudah anda tebak adalah: ')
                print(', '.join(listHurufSudahDitebak))

        if salah == 8:
            skor += 0
            print('Skor:', skor)
            print('Kata:', kata)
            menang = False

    save_kata_ditebak("Kata ditebak-{}.csv".format(username))
    save_data_skor()
    save_data_akun()
    return


def mainHangman2P():
    global poin2P, skor2P, akun_aktif, akun_lawan, maksRonde2P, bonus1, bonus2, bonus4

    print("\nLogin pemain kedua")
    akun_lawan = login([akun_aktif["index"]])["index"]

    pemain = [akun_aktif["index"], akun_lawan]

    def pemenang_poin():
        # return -1 jika belum ada pemenang
        # return -2 jika seri
        # selain itu return indeks pemain yang menang
        if (max(poin2P) <= maksRonde2P / 2): return -1
        if (poin2P[0] == poin2P[1]): return -2
        for i in [0, 1]:
            if (poin2P[i] > maksRonde2P / 2):
                return pemain[i]
        return -1

    def bonus_4(progress):
        for i in range(len(kata)):
            if kata[i] == " ":
                progress += " "
            else:
                progress += "_"
        return progress

    def nama_pemain(indeks):
        return list_akun[pemain[indeks]]["username"]

    def print_poin():
        print(f'Poin: {nama_pemain(0)} {poin2P[0]}-{poin2P[1]} {nama_pemain(1)}')

    def print_skor():
        print(f'Skor: {nama_pemain(0)} {skor2P[0]}-{skor2P[1]} {nama_pemain(1)}')

    def print_hangman():
        for i in [0, 1]:
            print(f'Pemain {i + 1} ({nama_pemain(i)}):')
            print(gambarHangman(salah[i]))

    def save_data_skor():
        for i in [0, 1]:
            if int(list_akun[pemain[i]]["skor"]) < skor2P[i]:
                print(
                    f"{nama_pemain(i)}, skor anda lebih tinggi dari sebelumnya. Apakah anda ingin menyimpannya? (Ya/Tidak)")
                saveskor = input("> ").upper()
                if saveskor == "YA":
                    list_akun[pemain[i]]["skor"] = int(skor2P[i])
                elif saveskor == "TIDAK":
                    list_akun[pemain[i]]["skor"] = int(list_akun[pemain[i]]["skor"])
                else:
                    save_data_skor()
            buffer = list_akun[pemain[i]]
            sortir_naik(list_akun[pemain[i]])
            pemain[i] = buffer["index"]

    skor2P = [0, 0]
    mainLagi = True
    while (mainLagi):
        poin2P = [0, 0]
        ronde = 1
        giliran = 0

        while (ronde <= maksRonde2P and pemenang_poin() == -1):
            if bonus2 == True:
                while True:
                    print('             Kategori             ')
                    print('Kota, Negara, Hewan, Buah, Sayuran')
                    kategori = (str(input('Masukkan kategori yang anda inginkan: '))).upper()
                    if(kategori in list(dict_kata.keys())):
                        break
                    else:
                        print('Kategori tidak valid!')
            else:
                kategori = kategoriRandom(dict_kata)

            kata = kataRandom(dict_kata, kategori)
            progress = ""
            if bonus4 == True:
                progress = bonus_4(progress)
            else:
                progress = "_" * len(kata)

            bonusawal1 = bonus1

            salah = [0, 0]
            listHurufSudahDitebak = []

            print('Progres kata:', progress)
            print_hangman()

            while salah[0] < 8 and salah[1] < 8:
                if bonus1 == True:
                    print('Masukkan input "Hint" untuk memunculkan kategori kata')
                huruf = (str(input(f'{nama_pemain(giliran)}, masukkan tebakan huruf anda: '))).upper()

                if len(huruf) == 1:
                    if huruf in listHurufSudahDitebak:
                        print('\nMaaf, huruf ini sudah ditebak')
                    elif huruf in kata:
                        listHurufSudahDitebak.append(huruf)

                        listProgress = list(progress)
                        ex = [i for i, alfabet in enumerate(kata) if alfabet == huruf]

                        for i in ex:
                            listProgress[i] = huruf
                        progress = ''.join(listProgress)

                        print('\nSelamat! Anda menebak huruf yang benar')
                        print('Progres kata:', progress)
                        print_hangman()
                        print('List huruf yang sudah ditebak adalah: ')
                        print(', '.join(listHurufSudahDitebak))
                        giliran = 1 - giliran  # tukar giliran

                        if "_" not in progress:
                            if bonusawal1 == True:
                                bonus1 = True
                            else:
                                bonus1 = False
                            menang = -1 if salah[0] == salah[1] else 0 if salah[0] < salah[1] else 1
                            if (menang == -1):
                                for i in [0, 1]:
                                    poin2P[i] += 1
                                print('Seri!')
                            else:
                                poin2P[menang] += 1
                                print(f'{nama_pemain(menang)} menang di ronde ini!')
                            for i in [0, 1]: skor2P[i] += 100 - salah[i] ** 2
                            print_poin()
                            break
                    else:
                        listHurufSudahDitebak.append(huruf)
                        salah[giliran] += 1

                        print('\nMaaf, kata yang anda tebak salah')
                        print('Progres kata:', progress)
                        print_hangman()
                        print('List huruf yang sudah ditebak adalah: ')
                        print(', '.join(listHurufSudahDitebak))
                        giliran = 1 - giliran  # tukar giliran
                elif huruf == 'HINT' and bonus1 == True:
                    print('Kategori huruf adalah', kategori)
                    print('Progres kata:', progress)
                    print_hangman()
                    print('List huruf yang sudah anda tebak adalah: ')
                    print(', '.join(listHurufSudahDitebak))
                    bonus1 = False
                else:
                    print("Hanya menerima input 1 huruf!\n")

            if salah[1 - giliran] == 8:
                poin2P[giliran] += 1
                for i in [0, 1]: skor2P[i] += 100 - salah[i] ** 2
                print(f'{nama_pemain(giliran)} kalah!')
                print(f'{nama_pemain(1 - giliran)} mendapatkan poin untuk ronde ini')
                print_poin()
        
        print('Main lagi? (Ya/Tidak)')
        while True:
            rematch = input('> ').upper()
            if(rematch == 'YA'):
                mainLagi = True
                break
            elif(rematch == 'TIDAK'):
                mainLagi = False
                break
            else:
                print('Input salah!')
        

    print('\nPermainan selesai!')
    pemenang = -1 if skor2P[0] == skor2P[1] else 0 if skor2P[0] > skor2P[1] else 1
    skorKumulatif = 0
    for i in [0, 1]: skorKumulatif += skor2P[i]

    if (pemenang == -1):
        print('Seri! Kedua pemain mendapatkan setengah dari skor kumulatif!')
        for i in [0, 1]: skor2P[i] = skorKumulatif // 2
    else:
        print(f'{nama_pemain(pemenang)} memenangi permainan dan seluruh skor kumulatif!')
        skor2P[pemenang] = skorKumulatif
        skor2P[1 - pemenang] = 0
    print_skor()

    save_data_skor()
    save_data_akun()
    return

def tutorial1p():
    # KAMUS lokal:
    # -

    print('''Selamat datang di game Hangman! Hangman adalah sebuah game tebak kata dimana kamu menebak setiap huruf dari kata yang sudah ditentukan secara acak.
Cara bermain Single Player:
1. Tebak dan masukkan satu huruf yang ada di pikiran kamu
2. Jika huruf tebakanmu benar, huruf akan tertulis di permainan
3. Jika huruf tebakanmu salah, hangman akan tergambar sedikit demi sedikit setiap kesalahan.
4. Tebak dan masukkan kembali satu huruf yang berbeda hingga terbentuk kata yang telah ditentukan
5. Kamu mempunyai 8 kali kesempatan salah hingga hangman tergambar sempurna
6. Ketika hangman tergambar sempurna, maka permainan selesai dan kamu dianggap kalah :(
7. Ketika semua huruf dari kata telah ditebak dengan benar semua, maka kamu memenangkan permainan :D
8. Tambahan: kamu dapat mengaktifkan bonus-bonus yang tersedia, seperti: pilih kategori, hint, dan memunculkan tanda baca.
Good Luck Have Fun! Tekan ENTER untuk melanjutkan...''')
    input()


def tutorial2p():
    # KAMUS lokal:
    # -

    print(f'''Selamat datang di game Hangman! Hangman adalah sebuah game tebak kata dimana kamu menebak setiap huruf dari kata yang sudah ditentukan secara acak.
Cara bermain Multiplayer:
1. Kamu dan temanmu menebak huruf dari satu kata yang sama secara bergantian hingga semua huruf tertebak dengan benar
2. Kamu dan temanmu memiliki hangman masing-masing yang akan tergambar sedikit demi sedikit setiap kesalahan sendiri dalam menebak huruf
3. Player yang memiliki kesalahan penebakan yang lebih sedikit memenangkan ronde.
4. Permainan dilakukan dengan sistem ‘best of {maksRonde2P}’. Player yang memenangkan {maksRonde2P // 2 + 1} ronde terlebih dulu akan memenangkan permainan.
Good Luck Have Fun! Tekan ENTER untuk melanjutkan...''')
    input()



def mainmenu():
    # KAMUS lokal:
    # pilihan : int
    # valid : boolean

    while True:
        print("\n", "=" * 20, "HANGMAN GAME", "=" * 20)
        print('[1] Single player')
        print('[2] Multiplayer')
        print('[3] Profil')
        print('[4] Leaderboard')
        print('[5] Settings')
        print('[6] Exit')
        print()

        valid = False
        while not valid:
            try:
                pilihan = int(input('\nMasukkan input: '))
                valid = True
            except ValueError:
                print('Input salah!')

        print()

        if pilihan == 1:
            tutorial1p()
            mainHangman()
        elif pilihan == 2:
            tutorial2p()
            mainHangman2P()
        elif pilihan == 3:
            status()
        elif pilihan == 4:
            topTen()
        elif pilihan == 5:
            setting()
        elif pilihan == 6:
            print('Terima kasih telah bermain Hangman. Sampai jumpa!')
            return
        else:
            print('Input salah!')


def loginregister():
    # KAMUS lokal:
    # pilihan1 : int
    # valid : boolean

    global akun_aktif
    print("=" * 20, "HANGMAN GAME", "=" * 20)
    print('[1] Login')
    print('[2] Registrasi')

    valid = False
    while not valid:
        try:
            pilihan1 = int(input('\nMasukkan input: '))
            valid = True
        except ValueError:
            print('Input salah!')

    if pilihan1 == 1:
        akun_aktif = login()
        mainmenu()
    elif pilihan1 == 2:
        register()
        loginregister()
    else:
        print("input salah")

if __name__ == "__main__":
    open_data_kata()
    open_data_akun()
    loginregister()